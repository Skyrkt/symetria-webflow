// @author Ben Shackleton
// Code adapted from https://codepen.io/arkev/pen/pDdoL by @arkev

export default () => {
  var wrapper = document.querySelector('.snake-game');
  var canvas = document.createElement('canvas');
  canvas.width = 1000;
  canvas.height = 1000;
  canvas.style.width = '100%';
  canvas.style.cursor = 'pointer';
  wrapper.appendChild(canvas);
  var ctx = canvas.getContext("2d");
  var w = canvas.width;
  var h = canvas.height;

  var game_loop;
  var cw = 50;
  var d;
  var food;
  var score;

  var snake_array;

  function init()
  {
    d = "right";
    create_snake();
    create_food();
    score = 0;

    if(typeof game_loop != "undefined") clearInterval(game_loop);
    game_loop = setInterval(paint, 120);
  }
  init();

  canvas.addEventListener('click', (e) => {
    init();
  })

  function create_snake()
  {
    var length = 8;
    snake_array = [];
    for(var i = length-1; i>=0; i--)
    {
      snake_array.push({x: i, y:0});
    }
  }

  function create_food()
  {
    food = {
      x: Math.round(Math.random()*(w-cw)/cw),
      y: Math.round(Math.random()*(h-cw)/cw),
    };
  }

  function paint()
  {
    ctx.fillStyle = "#19191a";
    ctx.fillRect(0, 0, w, h);
    ctx.strokeStyle = "white";
    ctx.strokeRect(0, 0, w, h);

    var nx = snake_array[0].x;
    var ny = snake_array[0].y;

    if(d == "right") nx++;
    else if(d == "left") nx--;
    else if(d == "up") ny--;
    else if(d == "down") ny++;

    if(nx == -1 || nx == w/cw || ny == -1 || ny == h/cw || check_collision(nx, ny, snake_array))
    {

      var start_text = "START";
      ctx.font = "100px Integral CF";
      ctx.fillStyle = 'white';
      ctx.textAlign = 'center';
      ctx.fillText(start_text, 500, 500);
      //end game
      clearInterval(game_loop);
    }

    if(nx == food.x && ny == food.y)
    {
      var tail = {x: nx, y: ny};
      score++;

      create_food();
    }
    else
    {
      var tail = snake_array.pop();
      tail.x = nx; tail.y = ny;
    }

    snake_array.unshift(tail);

    for(var i = 0; i < snake_array.length; i++)
    {
      var c = snake_array[i];
      paint_cell(c.x, c.y);
    }

    paint_cell(food.x, food.y);

    var score_text = "SCORE: " + score;
    ctx.font = "36px Integral CF";
    ctx.textAlign = 'left';
    ctx.fillText(score_text, 20, h-25);
  }

  function paint_cell(x, y)
  {
    var cell_text = "*";
    ctx.font = "100px Integral CF";
    ctx.fillStyle = 'white';
    ctx.fillText(cell_text, x*cw, (y*cw + 70));
  }

  function check_collision(x, y, array)
  {
    for(var i = 0; i < array.length; i++)
    {
      if(array[i].x == x && array[i].y == y)
      return true;
    }
    return false;
  }

  document.addEventListener('keydown', (e) => {
    var key = e.which;
    console.log(key);

    if (key == '37' || key == '38' || key == '39' || key == '40') e.preventDefault();
    if(key == "37" && d != "right") d = "left";
    else if (key == "38" && d != "down") d = "up";
    else if (key == "39" && d != "left") d = "right";
    else if (key == "40" && d != "up") d = "down";
  });
}
