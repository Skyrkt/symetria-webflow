import './main.scss';
import Prompter from './vendor/prompter.js';
import StringStyle from './vendor/stringstyle.js';
// import Classcroll from './vendor/classscroll.js';
import snakeGame from './snakeGame.js';
// import gifController from './gifController.js';
import lottieController from './lottieController.js';

import lottie from 'lottie-web';
import anim01 from './lottie/01.json';
import anim02 from './lottie/02.json';
import anim03 from './lottie/03.json';
import anim04 from './lottie/04.json';

if (document.querySelector('.sym-block-slides')) {
  let bcSlide1 = lottie.loadAnimation({
    container: document.querySelector('#bcImage1 .video-wrapper'), // Required
    animationData: anim01, // Required
    renderer: 'svg', // Required (svg/canvas/html)
    loop: true, // Optional
    autoplay: true, // Optional
  })

  let bcSlide2 = lottie.loadAnimation({
    container: document.querySelector('#bcImage2 .video-wrapper'), // Required
    animationData: anim02, // Required
    renderer: 'svg', // Required (svg/canvas/html)
    loop: true, // Optional
    autoplay: true, // Optional
  })

  let bcSlide3 = lottie.loadAnimation({
    container: document.querySelector('#bcImage3 .video-wrapper'), // Required
    animationData: anim03, // Required
    renderer: 'svg', // Required (svg/canvas/html)
    loop: true, // Optional
    autoplay: true, // Optional
  })

  let bcSlide4 = lottie.loadAnimation({
    container: document.querySelector('#bcImage4 .video-wrapper'), // Required
    animationData: anim04, // Required
    renderer: 'svg', // Required (svg/canvas/html)
    loop: true, // Optional
    autoplay: true, // Optional
  })

  // Gif Animation Controller
  const animData = [
    {
      elem: '#bcSlide1',
      duration: 1000,
      add: 40,
      remove: 95,
      target: '#bcImage1',
      lottie: bcSlide1,
    },
    {
      elem: '#bcSlide2',
      duration: 1000,
      add: 26,
      remove: 95,
      target: '#bcImage2',
      lottie: bcSlide2,
    },
    {
      elem: '#bcSlide3',
      duration: 1000,
      add: 26,
      remove: 95,
      target: '#bcImage3',
      lottie: bcSlide3,
    },
    {
      elem: '#bcSlide4',
      duration: 1000,
      add: 26,
      remove: 60,
      target: '#bcImage4',
      lottie: bcSlide4,
    },
  ];

  if (window.innerWidth >= 1024) {
    // gifController('.sym-block-slides', animData);
    lottieController('.sym-block-slides', animData);
  }
}

// Animations and Styling effetcs
StringStyle('.textpopup');
StringStyle('.textpopup2', { interval: 500 });
StringStyle('.textpopup3', { interval: 800, deliminator: '|', keepDeliminator: false });
Prompter('.textprompt');

// Popup close and storage
const popup = document.querySelector('.sym-popup');
const popupClose = document.querySelector('.sym-popup__close-icon');
if (popup, popupClose) {
  popupClose.addEventListener('click', (e) => {
    e.preventDefault();
    popup.style.display = 'none';
    localStorage.setItem('popup-read', true);
  });

  if(!localStorage.getItem('popup-read')) {
    popup.style.display = 'flex';
  } else {
    popup.style.display = 'none';
  }
}

// TODO this should really be cleaned up, but hey we're dealing with a bizzare workflow!
const burger = document.querySelector('.sym-navbar__burger');
const menu = document.querySelector('.sym-menu');
const close = document.querySelector('.sym-menu__close');
const navbar = document.querySelector('.sym-navbar');
const asterix = document.querySelector('.sym-navbar__asterix');

if (burger, menu, close, navbar, asterix) {
  burger.addEventListener('click', () => {
    menu.classList.add('visible');
    burger.style.display = 'none';
    close.style.display = 'block';
    asterix.classList.add('white');
  })

  close.addEventListener('click', () => {
    menu.classList.remove('visible');
    burger.style.display = 'block';
    close.style.display = 'none';
    asterix.classList.remove('white');
  })
}

// Price ticker
const ticWrapper = document.querySelector('.sym-ticker');
const pushTickerData = (title, data) => {
  if (ticWrapper) {
    const block = document.createElement('div');
    block.classList.add('sym-ticker__block');

    const heading = document.createElement('div');
    heading.classList.add('sym-ticker__heading');
    heading.innerHTML = title;
    block.appendChild(heading);

    const value = document.createElement('div');
    value.classList.add('sym-ticker__value');
    value.innerHTML = data;
    block.appendChild(value);

    ticWrapper.appendChild(block);
  }
}

if (ticWrapper) {
  axios.get('https://api.coingecko.com/api/v3/simple/price?ids=litecoin%2Cbitcoin%2cdogecoin&vs_currencies=eth')
  .then(function (response) {
    pushTickerData('LITECOIN', `${response.data.litecoin.eth} ETH`);
    pushTickerData('DOGECOIN', `${response.data.dogecoin.eth} ETH`);
    pushTickerData('BITCOIN', `${response.data.bitcoin.eth} ETH`);
  })
  .catch(function (error) {
    console.warn(error);
  });
}

// Navbar trade button
// const tradeNowMenu = document.querySelector('.ticker-trade-now.menu');
// if (tradeNowMenu, ticWrapper) {
//   Classcroll('.sym-ticker', { class: 'visible', target: '.ticker-trade-now.menu', add: 99});
// }

// Navbar scroll display functionality
const symBlockSlides = document.querySelector('.sym-block-slides');

if (navbar && ticWrapper) {
  window.addEventListener('scroll', (e) => {
    const rect = ticWrapper.getBoundingClientRect().top;
    if (rect >= 0) {
      navbar.classList.remove('scrolled');
    } else {
      navbar.classList.add('scrolled');
    }
  })

} else if (navbar && symBlockSlides) {
  window.addEventListener('scroll', (e) => {
    const rect = symBlockSlides.getBoundingClientRect().top;
    if (rect >= 0) {
      navbar.classList.remove('scrolled');
    } else {
      navbar.classList.add('scrolled');
    }
  })

} else if (navbar) {
  window.addEventListener('scroll', (e) => {
    if (window.scrollY >= 1) {
      navbar.classList.add('scrolled');
    } else {
      navbar.classList.remove('scrolled');
    }
  })
}

// Snake Game
const snakeWrapper = document.querySelector('#snake-wrapper');
const snakeStartButton = document.querySelector('#snakeStartButton');
const snakeEndButton = document.querySelector('.snake-close');
let snakeGameActive = false;
if (snakeWrapper && snakeStartButton && snakeEndButton) {
  snakeStartButton.addEventListener('click', () => {
    if (window.innerWidth > 1024) {
      snakeWrapper.classList.remove('hidden');
      if (!snakeGameActive) {
        snakeGameActive = true;
        setTimeout(() => snakeGame(), 500);
      }
    }
  })

  snakeEndButton.addEventListener('click', () => {
    snakeWrapper.classList.add('hidden');
  })
}



