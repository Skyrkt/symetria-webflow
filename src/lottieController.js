// @author Ben Shackleton

// TODO: * Clean the ever loving crap up out of this, lol
//       * needs a refactor as the concept changed during implementation

// Shared Variables
let listening = false;
let wHeight = window.innerHeight;
let wWidth = window.innerWidth;
let gifs = [];
let newPos = null;
let currentPos = -1;
let playing = false;

// Object Class
class GifObject {
  constructor(data) {
    this.elem = document.querySelector(data.elem);
    this.target = document.querySelector(data.target);
    this.lottie = data.lottie;
    this.add = data.add;
    this.remove = data.remove;
    this.duration = data.duration;
  }

  pos() {
    const rect = this.elem.getBoundingClientRect();
    const height = wHeight + rect.height;
    const pos = Math.floor((height - (rect.top + rect.height)) / height * 100);
    return pos;
  }

  active() {
    return (this.pos() >= this.add && this.pos() <= this.remove) ? true : false;
  }
}

// Listeners
const initListeners = () => {
  window.addEventListener('resize', () => {
    wHeight = window.innerHeight;
    wWidth = window.innerWidth;

    if (wWidth >= 1024) {
      checkPosition();
    }
  })

  window.addEventListener('scroll', () => {
    if (wWidth >= 1024) {
      checkPosition();
    }
  })
}

const gifController = () => {
  if (newPos === currentPos) return;
  if (!playing) playGif();
}

const playGif = () => {
  playing = true;
  currentPos = newPos;

  Array.from(gifs).forEach((gif, key) => {
    if (key === currentPos) {
      gif.lottie.play();
      gif.lottie.loop = false;
      gif.target.classList.add('active');
    }
  })

  setTimeout(() => {
    playing = false;
    gifController();
  }, 0)
}

// Helpers
const checkPosition = () => {
  let active = [-1];
  Array.from(gifs).forEach((gif, key) => {
    if (gif.active()) {
      active.push(key);
    } else {
      gif.target.classList.remove('active');
      gif.lottie.stop();
    }
  })
  active = active.pop();
  if (active >= 0) {
    newPos = active;
    gifController();
  } else {
    currentPos = -1;
  }
}

// Export
export default (wrapper, data) => {
  wrapper = document.querySelector(`${wrapper}`);
  if (wrapper) {
    data.forEach(item => {
      const gif = new GifObject(item);
      gifs.push(gif);
    })

    if (!listening) {
      initListeners();
      listening = true;
      checkPosition();
    }
  }
}
