# Symetria Styles
Extra styling and js helpers for Webflow page.

`yarn install`, then `yarn start` to build `/dist` with minified css and js to add to Webflow CMS.

* There is a basic `index.html` page for development purposes before pushing code up to webflow.
* CSS is copied and pasted into the Webflow CMS "Project Settings > Custom Code" menu.
* Due to the larger size of the JS, the file is referenced in the "Project Settings > Custom Code" pointing to https://raw.githack.com/ CDN service. It takes serveral minutes to update. If you make changes, you'll need to purge the CDN cache.
